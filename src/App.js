import React, { Component } from 'react';
import './style/App.css';


function FriendsList(props) {
	return (
		<ul>
			{props.list.map((friend, id) => (
				<li key={friend.id}>
					<span>{friend.name}</span>
					<button onClick={() => props.onRemoveFriend(friend.id)}>Remove</button>
					<button onClick={() => props.onToggleActive(friend.id)}>Desativar</button>
				</li>
			))}
		</ul>
	)
}

function ActiveFriends(props) {
	return(
		<ul>
			{props.list.map((friend,id) => (
				<li key={friend.id}>
					<span>{friend.name}</span>
					<button onClick={() => props.onRemoveFriend(friend.id)}>Remove</button>
					<button onClick={() => props.onToggleActive(friend.id)}>Desativar</button>
				</li>
			))}
		</ul>
	)
}

class App extends Component {

	constructor(props) {
		super(props)

		this.state = {
			friends: [
				{
					id: 1,
					name: 'Mateus',
					active: true
				},
				{
					id: 2,
					name: 'Larissa',
					active: true
				},
				{
					id: 3,
					name: 'Diego',
					active: false
				}
			],
			inputValue: ''
		}

		this.addFriend = this.addFriend.bind(this)
		this.updateInput = this.updateInput.bind(this)
		this.removeFriend = this.removeFriend.bind(this)
	}

	addFriend() {

		if(this.state.inputValue === '') {
			return
		}

		this.setState((prevState) => {
			return {
				friends: prevState.friends.concat([{
					id:  Math.floor((Math.random() * 1000000) + 5),
					name: this.state.inputValue
				}]),
				inputValue: ''
			}

		})
	}

	updateInput(e) {
		const value = e.target.value;

		this.setState({
			inputValue: value
		})
	}

	removeFriend(id) {
		this.setState((prevState) => {
			return {
				friends:prevState.friends.filter((friend) => friend.id !== id)
			}
		})
	}

	toggleActive(id) {
		this.setState((currentState) => {
			const friend = currentState.friends.find((friend) => friend.id === id)

			return {
				friends: {
					
				}
			}
		})
	}

	render() {
		return (
			<div className="app">
				<form action='#'>
					<input
						type='text'
						placeholder='Add a new friend'
						value={this.state.inputValue}
						onChange={this.updateInput}
					/>
					<button
					type='submit'
					onClick={this.addFriend}>
						Adicionar
					</button>
				</form>
				<FriendsList
					list = {this.state.friends}
					onRemoveFriend = {this.removeFriend}
				/>

				<ActiveFriends
					list={this.state.friends.filter((friend) => friend.active === true)}
					onToggleActive = {this.toggleActive}
				/>
			</div>
		);
	}
}

export default App;
